var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var autoprefixeer = require('gulp-autoprefixer');

/* SASS */
gulp.task('default', function () {
  return gulp
      .src('src/scss/qtg-main.scss')
      .pipe(sassGlob())
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(autoprefixeer({
        browsers: ['last 2 versions'],
        cascade: false
      }))
      .pipe(gulp.dest('../dist/styles')),
      gulp.src('src/scss/partials/base/_qt-grid-mixins.scss')
      .pipe(gulp.dest('../dist/mixins'));
});