#QUALITY THINK SASS FRAMEWORK

## USAGE

---

### Building and implementing

First you need to run `cd .\frontend\ && npm install`

To build the css, make desired changes to the settings.sccs-file and run `gulp` in the frontend-folder.

Implement the framework in either way:
- Add the mixins files from ./dist/mixins to your project
- Link the css-file from ./dist/styles in your project

Best practice is to copy the mixins files and use them in your sass.

### Add grid 
```
.classname {
  @include qt-grid(int $grid-columns, bool $dense-positioning, bool $image-fill, prop $grid-gap);
}
```

- Add fallback rules after grid
`@include qt-grid-ie-fallback(classname);`

- Markup
```
<div class="classname">
  <div class="classname__w12"></div> -- 12 columns wide
  <div class="classname__w6"></div> -- 6 columns wide
</div>
```
---
### Add button
`@include qt-button(str $style (round, semiround, null), prop $bg, prop $color, bool $hover, bool $animation)`


